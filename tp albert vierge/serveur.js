const {Tache, Calendrier} = require('./calendrier.js');
const html = require('./calendrierHtml.js');
const http = require('http');

const planningArthur = new Calendrier('Arthur');
planningArthur.addTask({titre:'Kick-Off', description:'Démarrage projet', debut: new Date(Date.UTC(2020, 09, 01, 06, 30, 0)),fin: new Date(Date.UTC(2020, 09, 01, 08, 30, 0))});
planningArthur.addTask({titre:'TN Meeting', description:'Point technique', debut: new Date(Date.UTC(2020, 09, 06, 12, 00, 0))}) ;
planningArthur.addTask({titre:'CRA', description:'Weekly report', debut: new Date(Date.UTC(2020, 09, 09, 15, 00, 0))}) ;
planningArthur.addTask({titre:'Off', description:'Congés', debut: new Date(Date.UTC(2020, 09, 07, 06, 30, 0)),fin : new Date(Date.UTC(2020, 09, 08, 19, 00, 0))}) ;
const planningBernard = new Calendrier('Bernard');
planningBernard.addTask({titre:'Information', description:'Information hebdomadaire', debut: new Date(Date.UTC(2020, 09, 02, 08, 00, 0)),fin: new Date(Date.UTC(2020, 09, 02, 08, 30, 0))});
planningBernard.addTask({titre:'Copil', description:'Réunio de copilotage', debut: new Date(Date.UTC(2020, 09, 04, 14, 00, 0))}) ;

let tabPlannings = new Array(planningArthur,planningBernard)

function trouverPlanning(nom){
  return tabPlannings.find(p => p.nom.toUpperCase() == nom.toUpperCase());
}

function trouverTaches(planning, nom){
  return planning.taches.find(t => t.titre.toUpperCase() == nom.toUpperCase());
}

let htmlContent = '';
htmlContent += html.build_page_liste_planning(tabPlannings);

const port = 8080;
const server = http.createServer(
    (req, res) => {
      // Renvoyez une réponse avec un statut 404 et une 'reason phrase' "Perdu.e.s ?"
      res.statusCode = 200
      res.statusMessage = ('pas perdu.e.s?')
      res.setHeader('Content-Type', 'text/html');

      const r_url = new URL(req.url, 'http://${req.headers.host}');
      //console.log(r_url.pathname);
      if (r_url.pathname === '/') {
        res.end(htmlContent)
      } else {
        planningName = r_url.pathname;
        planningName = planningName.substr(1);
        currentPlanning = trouverPlanning(planningName);
        if (currentPlanning === undefined) {
          res.statusCode = 404;
          res.end('<h1>Error 404</h1>');
          return;
        }
        res.end(html.build_page_planning(currentPlanning));
      }
    }
);

server.listen(
    port,
    () => {
      console.log(`Server running at port ${port}`);
    }
);

