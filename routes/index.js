var express = require('express');
var router = express.Router();

const planningArthur = { name: 'arthur', tache: [{ titre: 'Kick-Off', description: 'Démarrage projet' }] }
const planningBernard = { name: 'bernard', tache: [{ titre: 'vaisselle', description: 'il adore ça' }] }

const plannings = [planningArthur, planningBernard]

/* GET home page. */
router.get('/', function (req, res, next) {
  res.send(plannings);
});


/*localhost:3000/name/nompersonne */
router.get('/name/:name', function (req, res, next) {
  const planningTotal = []

  plannings.forEach(function (element, index) {
    if (element['name'] === req.params.name) {
      planningTotal.push(plannings[index])
    }
  });

  if (planningTotal.length == 0) {
    res.send("cette personne n'est pas inscrite")
  } else {
    res.send(planningTotal)
  }

});

/*localhost:3000/planning*/
router.post('/planning', function (req, res, next) {
  plannings.push({ name: req.body.name, tache: [{ titre: req.body.titre, description: req.body.description }] })
  res.send('planning créé');
});

/*localhost:3000/tache/nompersonne  */


router.post('/tache/:name', function (req, res, next) {
  plannings.forEach(function (element, index) {
    if (element['name'] === req.params.name) {
      // console.log(plannings[index])
      plannings[index]['tache'].push({ titre: req.body.titre, description: req.body.description })
    }
  });

  res.send(plannings)
});

/*localhost:3000/arthur/tache/Kick-off */
router.get('/:name/tache/:titre', function (req, res, next) {
  var tosend = []
  plannings.forEach(function (element1) {

    if (element1['name'] === req.params.name) {

      element1['tache'].forEach(function (element2) {

        if (element2['titre'] === req.params.titre) {

          tosend.push({ title: element2['titre'], description: element2['description'] })
        }
      })


    }
  });

  res.send(tosend)
});

/* localhost:3000/arthur */
router.delete('/:name', function (req, res, next) {
  plannings.forEach(function (element, index) {
    if (element['name'] === req.params.name) {
      plannings.splice(index, 1)
    }

  })
  res.send(plannings)
})


router.delete('/:name/:titre', function (req, res, next) {

  plannings.forEach(function (element1, index1) {

    if (element1['name'] === req.params.name) {

      element1['tache'].forEach(function (element2, index2) {
        if (element2['titre'] === req.params.titre) {

          console.log(plannings[index1]['tache'])
          plannings[index1]['tache'].splice(index2, 1)

        }
      })


    }
  });

  res.send(plannings)
});

module.exports = router;
